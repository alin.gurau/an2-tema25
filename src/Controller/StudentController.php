<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Student;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class StudentController extends AbstractController
{
    /**
     * @Route("/student", name="student")
     */
    public function new(Request $request)
    {
        $student = new Student();

        $form = $this->createFormBuilder($student)
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('phoneNumber', IntegerType::class)
            ->add('CNP', IntegerType::class)
            ->add('yearOfStudy', IntegerType::class)
            ->add('division', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Student'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $student = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($student);
            $entityManager->flush();

            return new Response('Student created successfully!');
        }

        return $this->render('student/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
